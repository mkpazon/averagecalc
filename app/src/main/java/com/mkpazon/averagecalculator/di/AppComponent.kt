package com.mkpazon.averagecalculator.di

import com.mkpazon.averagecalculator.ui.main.MainFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [PresenterModule::class, AverageCalculatorModule::class])
interface AppComponent{

    fun inject(target: MainFragment)

}