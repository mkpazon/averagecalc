package com.mkpazon.averagecalculator.di

import com.mkpazon.averagecalculatorsdk.lib.TheCalculator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AverageCalculatorModule {

    @Provides
    @Singleton
    fun provideAverageCalculatorSDK() = TheCalculator()
}