package com.mkpazon.averagecalculator.di

import com.mkpazon.averagecalculator.ui.main.MainPresenter
import com.mkpazon.averagecalculatorsdk.lib.TheCalculator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideMainPresenter(averageCalculator: TheCalculator) = MainPresenter(averageCalculator)
}