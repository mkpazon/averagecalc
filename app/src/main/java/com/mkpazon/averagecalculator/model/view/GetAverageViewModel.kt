package com.mkpazon.averagecalculator.model.view

data class GetAverageViewModel(val average: Float)