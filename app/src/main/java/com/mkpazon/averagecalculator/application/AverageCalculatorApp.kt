package com.mkpazon.averagecalculator.application

import android.app.Application
import com.facebook.stetho.Stetho
import com.mkpazon.averagecalculator.di.AppComponent
import com.mkpazon.averagecalculator.di.DaggerAppComponent
import timber.log.Timber

class AverageCalculatorApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        initTimber()
        initStetho()
        initDagger()
    }

    private fun initTimber() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return String.format("[%s#%s:%s]", super.createStackElementTag(element), element.methodName, element.lineNumber)
            }
        })
    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
    }


    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().build()
    }
}