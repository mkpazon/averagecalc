package com.mkpazon.averagecalculator.data

import com.mkpazon.averagecalculator.model.view.GetAverageViewModel
import io.reactivex.Flowable

interface RepositoryDataSourceInterface {

    fun getAverage(): Flowable<GetAverageViewModel>

}