package com.mkpazon.averagecalculator.data

import com.mkpazon.averagecalculator.model.view.GetAverageViewModel
import com.mkpazon.averagecalculatorsdk.lib.TheCalculator
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RepositoryDataSourceImpl @Inject constructor(var averageCalculator: TheCalculator) : RepositoryDataSourceInterface {

    override fun getAverage(): Flowable<GetAverageViewModel> {
        return averageCalculator.getAverage()
                .subscribeOn(Schedulers.io())
                .flatMap { result ->
                    Flowable.just(GetAverageViewModel(result))
                }
    }
}