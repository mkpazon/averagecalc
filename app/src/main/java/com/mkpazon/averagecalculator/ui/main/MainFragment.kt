package com.mkpazon.averagecalculator.ui.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkpazon.averagecalculator.R
import com.mkpazon.averagecalculator.application.AverageCalculatorApp
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import timber.log.Timber
import javax.inject.Inject

class MainFragment : Fragment(), MainView, View.OnClickListener {

    @Inject lateinit var mPresenter: MainPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Timber.d(".onCreateView")

        val view = inflater.inflate(R.layout.fragment_main, container, false)
        view.buttonSubmit.setOnClickListener(this)
        view.buttonGetAverage.setOnClickListener(this)

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Timber.d(".onCreate")
        super.onCreate(savedInstanceState)

        (activity?.application as AverageCalculatorApp).appComponent.inject(this)

        mPresenter.setView(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.buttonSubmit -> {
                val value = editTextAddNumber.text.toString()
                mPresenter.onSubmitClicked(value)
            }
            R.id.buttonGetAverage -> mPresenter.onGetAverageClicked()
        }
    }

    override fun showAverage(average: Float) {
        // TODO [For improvement] Move to strings.xml
        textViewMessage.text = "Average is $average"
    }

    override fun showErrorInvalidInput() {
        textViewMessage.text = getString(R.string.error_invalid_input)
    }

    override fun showAddNumberSuccess(num: Long) {
        // TODO [For improvement] Move to strings.xml
        textViewMessage.text = "Successfully added $num"
    }
}