package com.mkpazon.averagecalculator.ui.main

import com.mkpazon.averagecalculatorsdk.lib.TheCalculator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subscribers.DisposableSubscriber
import timber.log.Timber
import javax.inject.Inject

class MainPresenter @Inject constructor(private val aveCalc: TheCalculator) {

    private var mView: MainView? = null

    fun setView(mainView: MainView) {
        mView = mainView
    }

    fun onSubmitClicked(value: String) {
        Timber.d(".onSubmitClicked")
        try {
            val num = value.toLong()
            aveCalc.addNumber(num)
            mView?.showAddNumberSuccess(num)
        } catch (e: NumberFormatException) {
            mView?.showErrorInvalidInput()
        }
    }

    fun onGetAverageClicked() {
        Timber.d(".onGetAverageClicked")
        // TODO clear disposable

        aveCalc.getAverage()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSubscriber<Float>() {
                    override fun onComplete() {
                        Timber.d(".onComplete")
                    }

                    override fun onNext(average: Float?) {
                        Timber.d(".onNext")
                        if (average != null) {
                            mView?.showAverage(average)
                        }
                    }

                    override fun onError(t: Throwable?) {
                        Timber.e(t, ".onError")
                    }
                })
    }
}