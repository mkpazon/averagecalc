package com.mkpazon.averagecalculator.ui.main

interface MainView {
     fun showAverage(average: Float)
     fun showErrorInvalidInput()
     fun showAddNumberSuccess(num: Long)
}