package com.mkpazon.averagecalculatorsdk.lib

import com.mkpazon.averagecalculator.data.RestAdapter
import com.mkpazon.averagecalculatorsdk.lib.data.NetworkModule
import io.reactivex.Flowable

class TheCalculator {

    private val numbers: MutableList<Long> = mutableListOf()
    private val restAdapter: RestAdapter = NetworkModule.provideRestAdapter()

    fun addNumber(vararg numbers: Long) {
        if (!numbers.isEmpty()) {
            this.numbers.addAll(numbers.toList())
        }
    }

    fun getAverage(): Flowable<Float> {
        return restAdapter.getDefaultNumbers()
                .flatMap { defaultNumbers ->

                    var sum = 0L
                    var size = numbers.size

                    for (num in numbers) {
                        sum += num
                    }

                    for (num in defaultNumbers) {
                        sum += num
                    }
                    size += defaultNumbers.size

                    Flowable.just((sum / size).toFloat())
                }
    }
}