package com.mkpazon.averagecalculatorsdk.lib.data

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.mkpazon.averagecalculator.data.RestAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

private const val API_ROOT_URL = "https://roktcdn1.akamaized.net/"


object NetworkModule {

    fun provideRestAdapter(): RestAdapter {
        val objectMapper = NetworkModule.provideObjectMapper()
        val okHttpClient = NetworkModule.provideOkhttpClient()
        val retrofit = NetworkModule.provideRetrofit(okHttpClient, objectMapper)
        return RestAdapter(retrofit)
    }

    private fun provideRetrofit(client: OkHttpClient, objectMapper: ObjectMapper): Retrofit {
        return Retrofit.Builder()
                .client(client)
                .baseUrl(API_ROOT_URL)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private fun provideObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
        return objectMapper
    }

    private fun provideOkhttpClient(): OkHttpClient {
        val okhttpBuilder = OkHttpClient.Builder()
        okhttpBuilder.addNetworkInterceptor(StethoInterceptor())
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okhttpBuilder.addInterceptor(httpLoggingInterceptor)
        return okhttpBuilder.build()
    }
}