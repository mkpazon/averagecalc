package com.mkpazon.averagecalculator.data

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.http.GET

class RestAdapter constructor(retrofit: Retrofit) {
    private val webservice: Webservice

    interface Webservice {

        @GET("store/test/android/prestored_scores.json")
        fun getDefaultNumbers(): Flowable<LongArray>
    }

    fun getDefaultNumbers(): Flowable<LongArray> {
        return webservice.getDefaultNumbers()
                .subscribeOn(Schedulers.io())
    }

    init {
        webservice = retrofit.create(Webservice::class.java)
    }
}